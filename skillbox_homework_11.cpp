#include <iostream>

// returns line size in characters with an option to include \0
const size_t length(const char* str, const bool withzero = false)
{
    size_t i = 0;
    while (str[i])
        ++i;
    return withzero? ++i : i;
}

// Puts a c-string ended with an optional new line to std::ostream
void println(const char* ln, const bool withnew = false)
{
    if (!ln) // super unnecessary check for nullptr
        std::cout << "ERROR: string parameter is invalid or null\n";
   
    std::cout << ln;
  
    if (withnew)
        std::cout << '\n';
}

// entry point; nuff said
int main()
{
    static const char* message[]{ "Hello", ", ", "World", "!", NULL };

    for (size_t i = 0; message[i] != NULL; ++i)
    {
        if (i == 2)
        {
            println("Skillbox");
            continue;
        }
        println(message[i]);
    }
	
    std::cout << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
    std::cout << "Did you knew, that Skillbox is bigger than a World?\n";
    std::cout << "\"Skillbox\" size is " << length("Skillbox") << " characters, but \"World\" is just " << length("World") << " :)\n";
    std::cout << "\n\tHave a good day!\n";

	return 0;
}

